+++
title = "Participation"
description = "Volunteering, Speaking and Attending"
keywords = ["tickets","call for papers","volunteer","organiser"]
id = "register"
+++

<br>
### <i class="fas fa-chalkboard-teacher"></i> Call for Tutorial Proposals
The call for proposals is now [live on our website](/news/2019/09/28/call-for-tutorials/)! We will begin accepting submissions on October 15th, and close the submission portal on November 25th. We plan to notify tutorial applicants of acceptance in early January, prior to the opening of registration.

<br>
### <i class="fas fa-comment"></i> Call for Papers
We anticipate the call for papers being posted on November 15th, 2019. The deadline for abstracts will be February 3rd, 2020. We will open the portal for accepting abstracts when the call for papers is posted, and announce acceptances on March 16th, 2020.

<br>
### <i class="fas fa-user-plus"></i> Registration
We anticipate registration opening on January 15th, 2020, though we will announce pricing this fall.

<br>
### <i class="fas fa-users"></i> Diversity Scholarships
As with past useR! conferences, we will be providing diversity scholarships that subsidize travel and include a full registration ticket (including two tutorials and the gala dinner events). There will also be a dedicated networking event for diversity scholarship recipients with the conference's sponsors. We anticipate the call for diversity scholarship applicants being posted on November 15th, 2019. The deadline for applications will be February 3rd, 2020. We will open the portal for applications when the call is posted, and announce recipients on March 16th, 2020.
