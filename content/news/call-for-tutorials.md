+++
title = "Call for Tutorials"
date = "2019-09-28T00:00:00"
tags = ["Tutorials"]
categories = ["Tutorials"]
banner = "news/call-for-tutorials.png"
+++

# Call for Proposals of Tutorials at UseR! 2020

## Overview

The useR! 2020 committee is seeking submissions for tutorials to be hosted on the first day of the conference. The intended audience for these tutorials includes the useR! 2020 attendees, coming from a variety of academic backgrounds and levels of preparation and usage level of R. Each tutorial session will be 3.5 hours long, with a 30-minute break dividing the session into roughly two equal halves (audience is welcome to take additional breaks as needed). The instructor is expected to have fluency in the topic of the tutorial and in teaching the topic to an audience coming from a diverse range of academic, professional, and other backgrounds.

## Topical areas

The following is a recommended, though not exhaustive, list of topics/areas to consider for a tutorial:

1. Bioinformatics
2. Cheminformatics
3. GIS, mapping, and/or spatial statistics
4. Analysis of text/qualitative data
5. Exploratory and confirmatory factor analysis, computing reliability of survey instruments, etc.
6. Psychometric analysis
7. Data visualization and exploratory data analysis
8. Machine learning
9. Interactive dashboards
10. Sports analytics
11. Neuroscience-related applications
12. R-Python interop, R-C++ interop, etc.
13. Web applications and APIs using R
14. Reproducible research (workflow, project files, sharing, version control, etc.)
15. (Social) Network analysis
16. Best practices in teaching R programming (beyond using it in analyses)
17. Functional programming in R
18. Analysis of high-dimensional and Big Data
19. Causal inference using R
20. Bayesian analysis (of cross-sectional and/or longitudinal data)

## Format for submitting a proposal

Your proposal should be submitted as a PDF (or Word) document, which has the following sections, with specific points addressed in each of the sections:

***Title​***: 10-20 words, describing succinctly the topic of the tutorial.

***Audience​***: 50-100 words, describing the expected background (subject matter and R experience) that an attendee should have in order to have a meaningful learning experience.
Instructor background:​ ~100 words, describing the instructor’s domains of expertise, experience teaching courses/workshops/tutorials using R, and how the instructor’s background has prepared them to teach the proposed tutorial. Provide the URL to your LinkedIn, Github, or any other site where details of your academic/professional background that are relevant to the proposed tutorial are available.

***Domain​***: 10-20 words, describing the subject areas (e.g. Social Sciences, Neuroscience, Geo-spatial statistics, etc.) to which the concepts and examples presented belong. This description will allow potential attendees determine whether the tutorial would be of interest to them.

***Points of appeal​***: ~75 words, describing what makes the proposed tutorial appealing to either audience from a broad range of backgrounds. If the tutorial content draws on examples from multiple fields/disciplines, the fields are identified and the tutorial is described in a way that is appealing to audience from those and related disciplines.

***Learning objectives***:​ 100 – 200 words, describing the specific concepts and skills the audience can expect to have learned by attending the tutorial. Be sure to specify each learning objective succinctly. A well-drafted proposal would probably have 3-5 well-stated learning objectives.

***Computing requirements***:​ ~50 words, describing the minimum hardware configuration and software attendees should bring to the tutorial to benefit from the hands-on exercises given during the tutorial.

***Teaching assistant​***: will you be able to identify one-two individuals who can serve as teaching assistants? Will you need the conference committee to identify volunteers who can serve as teaching assistants? What subject matter expertise should such an individual have to help with any difficulties that the attendees might face in running the code examples being demonstrated?
Lesson plan​: ~150 words, describing via a bulleted list, a sketch of the sequence of activities (theory/domain explanation, demonstration of code, hands-on work time, formal/informal Q+A, etc.) that the instructor expects to follow for delivering the learning experience to attendees.

***Expected level of audience’s R background***:​ ~50 words, a designation of beginner, intermediate, or advanced, and a short description of expected R-related skills.
Other considerations:​ ~ 100 words, describing any other constraints/needs the instructor might have that are relevant in delivering the learning experience to the attendees. If there are additional resources (available online) that the instructor things can help the audience to continue their learning beyond the tutorial, they can be included here, too.

## Honorarium

Instructors would receive an honorarium of USD $500. Teaching assistants would receive a pass to attend one tutorial of their choice (other than the one for which they would be serving as a teaching assistant).

The useR! 2020 committee is seeking submissions for tutorials to be hosted on the first day of the conference. The intended audience for these tutorials includes the useR! 2020 attendees, coming
from a variety of academic backgrounds and levels of preparation and usage level of R.

Each tutorial session will be 3.5 hours long, with a 30-minute break dividing the session into roughly two equal halves (audience is welcome to take additional breaks as needed).

The instructor is expected to have fluency in the topic of the tutorial and in teaching the topic to an audience coming from a diverse range of academic, professional, and other backgrounds.