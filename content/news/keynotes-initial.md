+++
title = "Initial Keynotes Announced"
date = "2019-09-28T00:00:00"
tags = ["Keynotes", "Speakers"]
categories = ["Keynotes"]
banner = "news/keynotes-initial.png"
+++

We are very excited to announce the first three of our six keynote speakers!

![Erin LeDell, Ph.D.](/news/erin.png)
## Erin LeDell, Ph.D.

Erin LeDell is the Chief Machine Learning Scientist at H2O.ai, the company that produces the open source, distributed machine learning platform, H2O. She received her Ph.D. from UC Berkeley where her research focused on machine learning and computational statistics. Erin is the founder of Women+ in Machine Learning & Data Science, and one of the co-founders of RLadies Global.

## Martin Mächler, Ph.D.

Martin Mächler is the Secretary General of the R Foundation and a member of the R Core Development team. He is a Senior Scientist and Lecturer at ETH Zurich's Seminar for Statistics. Martin is also an active developer of over twenty R packages, including `Matrix` and `cluster`.

![Noam Ross, Ph.D.](/news/noam.png)
## Noam Ross, Ph.D.

Noam Ross is a Senior Research Scientist and computational disease ecologist at EcoHealth Alliance. He received his Ph.D. from UC Davis, is a founding editor for software review at ROpenSci, and is an instructor for the Software Carpentry and Data Carpentry foundations. Noam is also an active developer of a number of R packages, including `fasterize` and `redoc`.
